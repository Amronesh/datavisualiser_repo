import math
from datetime import datetime, timedelta
from statistics import mean
import numpy as np
from bokeh.models import ColumnDataSource
from bokeh.palettes import Cividis9
from bokeh.plotting import Figure
from bokeh.transform import linear_cmap
from bokeh.util.hex import hexbin
from classes import Car

import controls as c


def createCarListFromData(car_data, car_list, vin_list):
    for item in car_data:
        item_vin, item_time, item_lat, item_lon = item['vin'], item['ttlocationsource'], item['merc_lat'], item[
            'merc_lon']
        if item_lat == "" or item_lon == "" or item_vin == "" or item_time == "":
            continue
        if item_vin not in vin_list:
            newCar = Car(item_vin, [])
            newCar.addTimepos({'time': item_time, 'lat': item_lat, 'lon': item_lon})
            vin_list.append(item_vin)
            car_list.append(newCar)
        else:
            for existingCar in car_list:
                if existingCar.vin == item_vin:
                    existingCar.addTimepos({'time': item_time, 'lat': item_lat, 'lon': item_lon})

    # maxim = (car_list[0].vin, len(car_list[0].timepos))
    # for car in car_list:
    #     carVin, numberOfTimepos = car.vin, len(car.timepos)
    #     max_carVin, max_numberOfTimepos = maxim
    #     if(numberOfTimepos > max_numberOfTimepos):
    #         maxim=(carVin, numberOfTimepos)
    # print("HERE IS MAX: " + str(maxim))


def generateEnoughColors(basePalette, newPaletteList: list, requiredColors):
    numberOfPalettes = int(math.ceil(requiredColors / 256))
    for i in range(numberOfPalettes):
        newPaletteList.extend(list(basePalette))


def getEarliestAndLatestDates(carList):
    timeStampList = []
    for car in carList:
        for timeItem in car.timepos:
            timeStampList.append(timeItem['time'])
    earliestDate = min(timeStampList)
    latestDate = max(timeStampList)
    return earliestDate, latestDate


# Process user's choice from the radio button groups in the form of a String
# RETURNS: "CLEAR" || "ABSOLUTE" || "RELATIVE" || "QUICK_RELATIVE"
def getUserChoice():
    firstChoice = c.radioButtonGroup_Custom.active
    secondChoice = c.radioButtonGroup_AbsRel.active

    if firstChoice == 0:
        return "CLEAR"
    elif firstChoice == 5:  # firstChoice = Custom
        return "ABSOLUTE" if secondChoice == 0 else "RELATIVE"  # Return either based on secondChoice value
    else:
        return "QUICK_RELATIVE"


# Handles visibility of UI elements based on user choice (with getUserChoice())
# VIN Select and radioButtonGroup_Custom always visible
def updateUIState():
    userChoice = getUserChoice()
    if userChoice == "CLEAR" or userChoice == "QUICK_RELATIVE":
        _changeUIVisibility([c.spinnerMinutes, c.spinnerHours, c.spinnerDays, c.datePickerRight,
                             c.datePickerLeft, c.radioButtonGroup_AbsRel], False)
    elif userChoice == "ABSOLUTE":
        _changeUIVisibility([c.spinnerMinutes, c.spinnerHours, c.spinnerDays], False)
        _changeUIVisibility([c.radioButtonGroup_AbsRel, c.datePickerLeft, c.datePickerRight], True)
    elif userChoice == "RELATIVE":
        _changeUIVisibility([c.datePickerRight, c.datePickerLeft], False)
        _changeUIVisibility([c.radioButtonGroup_AbsRel, c.spinnerDays, c.spinnerHours, c.spinnerMinutes], True)
    return userChoice


# Helper function for updateUIState
def _changeUIVisibility(obj, val):
    for item in obj:
        item.update(visible=val)


def initializeUIState(carList, earliestDate, latestDate):
    carVins = []
    if carList != 0:
        for item in carList:
            carVins.append((item.vin, item.vin[0:12] + ".."))

    # Update various UI elements to their default value
    c.multiChoiceSelector.update(options=carVins)
    c.datePickerLeft.update(value=earliestDate.date(), min_date=earliestDate.date(), max_date=latestDate.date())
    c.datePickerRight.update(value=latestDate.date(), min_date=earliestDate.date(), max_date=latestDate.date())


def choiceSelectorProcessing(carList):
    toBeRemoved = []
    selectedCarList = c.multiChoiceSelector.value
    if len(selectedCarList) != 0:
        for item in carList:
            if item.vin not in selectedCarList:
                toBeRemoved.append(item)
        for item in toBeRemoved:
            carList.remove(item)


def selectByDates(carList, userChoice):
    if userChoice == "ABSOLUTE":
        startDate = datetime.strptime(c.datePickerLeft.value, '%Y-%m-%d')
        startDate.replace(hour=0, minute=0, second=0)
        endDate = datetime.strptime(c.datePickerRight.value, '%Y-%m-%d')
        endDate = endDate + timedelta(hours=23, minutes=59, seconds=59)
        _processCarListByAbsoluteDate(startDate, endDate, carList)
    elif userChoice == "RELATIVE":
        days = c.spinnerDays.value
        hours = c.spinnerHours.value
        minutes = c.spinnerMinutes.value
        _transformRelativeDateToAbsolute(days, hours, minutes, carList)
    elif userChoice == "QUICK_RELATIVE":
        selection = c.RadioButtonLabels_Custom[c.radioButtonGroup_Custom.active]
        _transformQuickRelativeDateToRelative(selection, carList)


def _transformQuickRelativeDateToRelative(selection, carList):
    if selection == "10m":
        _transformRelativeDateToAbsolute(0, 0, 10, carList)
    elif selection == "30m":
        _transformRelativeDateToAbsolute(0, 0, 30, carList)
    elif selection == "1h":
        _transformRelativeDateToAbsolute(0, 1, 0, carList)
    elif selection == "12h":
        _transformRelativeDateToAbsolute(0, 12, 0, carList)


def _transformRelativeDateToAbsolute(days, hours, minutes, carList):
    _endDate = datetime.now()
    _endDate.replace(microsecond=0)
    _startDate = _endDate - timedelta(days=days, hours=hours, minutes=minutes)
    _processCarListByAbsoluteDate(_startDate, _endDate, carList)


def _processCarListByAbsoluteDate(startDate, endDate, carList):
    for car in list(carList):
        for timestamp in list(car.timepos):
            if timestamp['time'].replace(tzinfo=None) < startDate or timestamp['time'].replace(tzinfo=None) > endDate:
                car.timepos.remove(timestamp)
                if len(car.timepos) == 0:
                    carList.remove(car)


def lnglat_to_meters(longitude, latitude):
    if isinstance(longitude, (list, tuple)):
        longitude = np.array(longitude)
    if isinstance(latitude, (list, tuple)):
        latitude = np.array(latitude)

    origin_shift = np.pi * 6378137
    easting = longitude * origin_shift / 180.0
    northing = np.log(np.tan((90 + latitude) * np.pi / 360.0)) * origin_shift / np.pi
    return (easting, northing)


def initializeRoute(p: Figure):
    lineSource = ColumnDataSource(dict(lat=[], lon=[]))
    return p.line(x='lat',
                  y='lon',
                  line_color='black',
                  line_dash='dashed',
                  line_alpha=0.8,
                  source=lineSource,
                  visible=False)


def drawRoute(carList, lineGlyph, lineSource):
    lat = []
    lon = []
    if len(carList) == 1:
        newlist = sorted(carList[0].timepos, key=lambda x: x['time'], reverse=False)
        for timeStamp in newlist:
            lat.append(timeStamp['lat'])
            lon.append(timeStamp['lon'])
        lineSource.data.update(dict(lat=lat, lon=lon))
        lineGlyph.update(visible=True)
    else:
        lineGlyph.update(visible=False)


def drawInformation(dictVins, dictTime, dictLats, dictLons, carList, lineGlyph, lineSource, circleSource, hexGlyph,
                    heatmapDataSource, heatmapBackground, heatMapHoverTool):
    toggleButtonCheck = c.showHeatmapToggle.active
    if toggleButtonCheck:
        # Update circle source to empty lists
        circleSource.data.update(vin=[], time=[], lat=[], lon=[])
        # Calculate bins, update the heatmap and make it visible
        hexSize = c.heatMapSlider.value * 15000 + 50000
        hexGlyph.glyph.size = hexSize
        bins = hexbin(np.array(dictLats), np.array(dictLons), hexSize)
        if len(bins['counts']) == 0:
            coloringMaxSize = 0
        else:
            coloringMaxSize = 2.5 * mean(bins['counts'])
        heatmapDataSource.data.update(q=bins['q'], r=bins['r'], counts=bins['counts'], index=range(len(bins['counts'])))
        heatMapHoverTool.renderers = [hexGlyph]
        hexGlyph.glyph.fill_color=linear_cmap('counts', 'Plasma256', 0, coloringMaxSize)
        heatmapBackground.update(visible=True)
        c.heatMapSlider.update(disabled=False)
        hexGlyph.update(visible=True)

    else:
        #Make the heatmap invisible
        hexGlyph.update(visible=False)
        heatmapBackground.update(visible=False)
        c.heatMapSlider.update(disabled=True)
        #Update car list with newest data
        circleSource.data.update(vin=dictVins, time=dictTime, lat=dictLats, lon=dictLons)
        drawRoute(carList, lineGlyph, lineSource)
