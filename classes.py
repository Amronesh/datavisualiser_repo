# Implementing car object with initializing method from JSON String
# Do not use directly if intending to create multiple Car instances from single JSON source
# You should iterate through the list and instantiate as such: car_list.append(Car(**car))
import json
from datetime import datetime


class Car(object):
    vin: str
    timepos: []

    def __init__(self, vin, timepos):
        self.vin = vin
        self.timepos = timepos
        for timeItem in timepos:
            timeItem['time'] = datetime.fromisoformat(timeItem['time'])

    @classmethod
    def from_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)

    def addTimepos(self, newTimePos):
        newTimePos['time'] = datetime.fromisoformat(newTimePos['time'])
        self.timepos.append(newTimePos)

    def __repr__(self):
        return f'<CAR {self.vin} : {len(self.timepos)}>'
