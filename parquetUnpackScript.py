import time
import timeit
from datetime import datetime
import json as js
import pandas as pd
import os
from pyarrow import json
import pyarrow.parquet as pq

from pyproj import transform, Proj


def valueCheck(string_list: list) -> bool:
    for string in string_list:
        if string == "":
            return False
        value_check = dict.fromkeys("0123456789.")
        return all(c in value_check for c in string)


start = time.perf_counter()
directory = "parquetFiles/"
# pq_file = "parquetFiles/parq_1.parquet"
# data = pd.read_parquet(pq_file, engine="auto")

outProj = Proj('epsg:3857')
inProj = Proj('epsg:4326')

files = []
for filename in os.listdir(directory):
    if filename.startswith("parq_"):
        complete_filename = os.path.join(directory, filename)
        files.append(complete_filename)

data = [pd.read_parquet(f, engine='auto') for f in files]
merged_data = pd.concat(data, ignore_index=True)

# vin, ttlocationsource, gpslat, gpslng

jsonData = merged_data.to_json(
    orient='records')

entry_data = js.loads(jsonData)
output_data = []
entry_data_number = len(entry_data)
index = 0
for entry in entry_data:
    item_vin, item_time, item_lat, item_lon = entry['vin'], entry['ttlocationsource'], entry['gpslat'], entry['gpslng']
    if not valueCheck([item_lat, item_lon]) or item_vin == "" or item_time == "":
        print("Invalid entry found. Removing..")
        index += 1
        continue
    print("Converting " + str(index) + " / " + str(entry_data_number) + " -> " +
          item_lat + " " + item_lon + " " + item_time)
    merc_lat, merc_lon = transform(inProj, outProj, entry['gpslat'], entry['gpslng'])
    output_data.append({'vin': item_vin, 'ttlocationsource': item_time, 'merc_lat': merc_lat, 'merc_lon': merc_lon})
    index += 1

result = [js.dumps(record) for record in output_data]

obj = open('parquetFiles/nd-dataset.json', 'r+')
obj.truncate(0)
obj.close()

with open('parquetFiles/nd-dataset.json', 'w') as obj:
    for i in result:
        obj.write(i + '\n')

table = json.read_json("parquetFiles/nd-dataset.json")

pq.write_table(table, 'parquetFiles/processed_data.parquet')

end = time.perf_counter()
print("Time elapsed: " + str(end - start))
