FROM continuumio/miniconda
LABEL maintainer="samirak93"

ENV BK_VERSION=2.3.3
ENV PY_VERSION=3.9
ENV NUM_PROCS=4
ENV BOKEH_RESOURCES=cdn

RUN apt-get install git bash
RUN conda config --append channels conda-forge
RUN conda config --append channels districtdatalabs
RUN git clone https://Amronesh@bitbucket.org/Amronesh/datavisualiser_repo.git /datavisualiser
#using a private repo

#RUN conda install --yes --quiet python=${PY_VERSION} pybase64 pyyaml jinja2 bokeh=${BK_VERSION} numpy numba yellowbrick scipy sympy "nodejs>=8.8" pandas scikit-learn pyproj pyarrow
RUN conda install --yes --quiet python=${PY_VERSION} bokeh=${BK_VERSION} numpy pyproj pandas pyarrow "nodejs>=8.8"
RUN conda clean -ay

EXPOSE 8080
EXPOSE 5006
EXPOSE 80

# Run
CMD bokeh serve \
    --allow-websocket-origin="*" \
    --index=/index.html \
    --num-procs=${NUM_PROCS} \
    datavisualiser