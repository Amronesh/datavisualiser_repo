from bokeh.models import TextInput, RadioButtonGroup, DatePicker, Spinner, \
    MultiChoice, DateRangeSlider, Toggle, Slider

# Input Controls
vinInput = TextInput(title="Search by VIN")
multiChoiceSelector = MultiChoice(title="Select Cars by VIN", options=[])

RadioButtonLabels_Custom = ["Clear", "10m", "30m", "1h", "12h", "Custom"]
RadioButtonLabels_AbsRel = ["Absolute", "Relative"]

radioButtonGroup_Custom = RadioButtonGroup(labels=RadioButtonLabels_Custom, active=0)
radioButtonGroup_AbsRel = RadioButtonGroup(labels=RadioButtonLabels_AbsRel, active=0, visible=False)

datePickerLeft = DatePicker(title='Start date', visible=False)
datePickerRight = DatePicker(title='End date', visible=False)

spinnerDays = Spinner(title="Days", low=0, high=364, step=1, value=0, visible=False)
spinnerHours = Spinner(title="Hours", low=0, high=24, step=1, value=1, visible=False)
spinnerMinutes = Spinner(title="Minutes", low=0, high=60, step=1, value=0, visible=False)

showHeatmapToggle = Toggle(label="Show heatmap", button_type="danger")
heatMapSlider = Slider(start=1, end=25, value=10, step=1, title="Change tile size")
