import random
import json
import pandas as pd
import numpy as np
import customFunctions as cF
import controls as c
from copy import deepcopy
from bokeh.io import output_file, curdoc, show
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, HoverTool, CategoricalColorMapper
from bokeh.plotting import figure
from bokeh.palettes import all_palettes, Plasma9
from bokeh.tile_providers import CARTODBPOSITRON, get_provider
from bokeh.util.hex import hexbin

# Ignore CARTODBPOSITRON and get_provider reference errors if you get them

output_file("app.html")

CIRCLETOOLTIPS = [
    ("VIN", "@vin"),
    ("Time", "@time")
]

HEATMAPTOOLTIPS = [
    ("Number of entries: ", "@counts")
]

# Loading parquet file and instantiating car objects after converting it to JSON
# File path for docker container. Use when building docker image
#pq_file = "datavisualiser/parquetFiles/processed_data.parquet"
# File path for local use
pq_file = "parquetFiles/processed_data.parquet"
data = pd.read_parquet(pq_file, engine="auto")
jsonData = data.to_json(
    orient='records')  # 'records' orientation will format it as an iterable list-like object. Required for
# instantiation
car_data = json.loads(jsonData)

car_list = []
car_list_vinlist = []

cF.createCarListFromData(car_data, car_list, car_list_vinlist)

colorPalette = all_palettes['Turbo'][256]

latList = []
lonList = []
for car in car_list:
    for timestamp in car.timepos:
        latList.append(timestamp['lat'])
        lonList.append(timestamp['lon'])

bins = hexbin(np.array(latList), np.array(lonList), 120000)
heatmapDataSource = ColumnDataSource(bins)

earliestDate, latestDate = cF.getEarliestAndLatestDates(car_list)
cF.initializeUIState(car_list, earliestDate, latestDate)
circleSource = ColumnDataSource(data=dict(vin=[], time=[], lat=[], lon=[]))

tile_provider = get_provider(CARTODBPOSITRON)

p = figure(x_range=(-2000000, 6000000), y_range=(-1000000, 7000000),
           x_axis_type="mercator", y_axis_type="mercator", sizing_mode='stretch_both', tools="pan,wheel_zoom,reset,save")
p.add_tile(tile_provider)

# lineGlyph = cF.initializeRoute(p)
lineSource = ColumnDataSource(dict(lat=[], lon=[]))
lineGlyph = p.line(x='lat',
                   y='lon',
                   line_color='black',
                   line_dash='dashed',
                   line_alpha=0.9,
                   line_width=2,
                   source=lineSource,
                   visible=False)

colorPaletteList = list()
cF.generateEnoughColors(colorPalette, colorPaletteList, len(car_list))
finalPalette = tuple(random.sample(colorPaletteList, len(colorPaletteList)))

color_mapper = CategoricalColorMapper(palette=finalPalette, factors=car_list_vinlist)
circleGlyph = p.circle(x='lat',
                       y='lon',
                       size=15,
                       fill_color={'field': 'vin', 'transform': color_mapper},
                       fill_alpha=0.7,
                       line_color='black',
                       line_alpha=0.4,
                       source=circleSource)

heatMapBackground = p.square(x=[-200],
                             y=[200],
                             size=20000,
                             color=Plasma9[0],
                             alpha=0.6,
                             visible=False)

hexGlyph = p.hex_tile(q="q",
                      r="r",
                      source=heatmapDataSource,
                      alpha=0.6,
                      hover_color="white",
                      hover_alpha=0.4,
                      visible=False)

circleHoverTool = HoverTool(renderers=[circleGlyph], tooltips=CIRCLETOOLTIPS)
heatMapHoverTool = HoverTool(renderers=[], tooltips=HEATMAPTOOLTIPS)
p.add_tools(circleHoverTool)
p.add_tools(heatMapHoverTool)
def update():
    carList = deepcopy(car_list)

    userChoice = cF.updateUIState()

    cF.choiceSelectorProcessing(carList)
    cF.selectByDates(carList, userChoice)
    # Initialize empty list to provide as a circleGlyph source update
    dictVins = []
    dictTime = []
    dictLats = []
    dictLons = []
    index = 0

    for item in carList:
        count = len(item.timepos)
        for i in range(count):
            itemVin, itemTime, itemLat, itemLon = item.vin, \
                                                  item.timepos[i]['time'].strftime("%Y-%m-%d %H:%M:%S"), \
                                                  float(item.timepos[i]['lat']), float(item.timepos[i]['lon'])
            dictVins.insert(index, itemVin)
            dictTime.insert(index, itemTime)
            dictLats.insert(index, itemLat)
            dictLons.insert(index, itemLon)

            index += 1

    cF.drawInformation(dictVins, dictTime, dictLats, dictLons, carList, lineGlyph, lineSource, circleSource, hexGlyph,
                       heatmapDataSource, heatMapBackground, heatMapHoverTool)
    # circleSource.data.update(vin=dictVins, time=dictTime, lat=dictLats, lon=dictLons)
    # circleSource.data.update(vin=[], time=[], lat=[], lon=[])
    # cF.drawRoute(carList, lineGlyph, lineSource)


controls = [c.multiChoiceSelector, c.showHeatmapToggle, c.heatMapSlider, c.radioButtonGroup_Custom,
            c.radioButtonGroup_AbsRel, c.datePickerLeft, c.datePickerRight,
            c.spinnerDays, c.spinnerHours, c.spinnerMinutes]

for control in controls:
    if control != c.radioButtonGroup_Custom and control != c.radioButtonGroup_AbsRel and control != c.showHeatmapToggle:
        control.on_change('value', lambda attr, old, new: update())
    else:
        control.on_change('active', lambda attr, old, new: update())

permanentInputs = column(c.multiChoiceSelector, c.radioButtonGroup_Custom, c.radioButtonGroup_AbsRel,
                         width=320)
absoluteInputs = row(c.datePickerLeft, c.datePickerRight, width=320)
relativeInputs = row(c.spinnerDays, c.spinnerHours, c.spinnerMinutes, width=320)
additionalInputs = column(c.showHeatmapToggle, c.heatMapSlider)
inputsMenu = column(permanentInputs, absoluteInputs, relativeInputs, additionalInputs, width=320)

l = column(row(inputsMenu, p), margin=(5, 5, 5, 5), sizing_mode="stretch_both")

update()
curdoc().add_root(l)
curdoc().title = "Data Visualiser"
show(l)
